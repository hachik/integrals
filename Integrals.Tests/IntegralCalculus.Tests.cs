using System;
using Xunit;

namespace Integrals.Tests
{
    public class IntegralCalculusTests
    {
        [Fact(DisplayName = "Works with sinus function")]
        public void WorksWithSinus()
        {
            //  arrange
            Func<double, double> func = Math.Sin;

            //  act
            double actual = IntegralCalculus.Calculate(func, 0, Math.PI, 0.01);

            //  assert
            Assert.InRange(actual, 1.99, 2.01);
        }

        [Fact(DisplayName = "Works with polynomial function")]
        public void WorksWithPolynomialFunction()
        {
            //  arrange
            Func<double, double> func = PolynomialFunction;

            //  act
            double actual = IntegralCalculus.Calculate(func, 5.4, 6.7, 0.01);

            //  assert
            Assert.InRange(actual, 162.75975, 162.77975);
        }

        [Theory(DisplayName = "Works with sinus alternating function")]
        [InlineData(0, 2 * Math.PI, 0.01, 0)]
        [InlineData(0.5 * Math.PI, 1.5 * Math.PI, 0.01, 0)]
        [InlineData(Math.PI / 3, (2/3D) * Math.PI, 0.01, 1)]
        [InlineData((4 / 3D) * Math.PI, (5 / 3D) * Math.PI, 0.01, -1)]
        [InlineData(-2 * Math.PI, 0, 0.01, 0)]
        [InlineData(-1.5 * Math.PI, -0.5 * Math.PI, 0.01, 0)]
        [InlineData(-(2 / 3D) * Math.PI , - Math.PI / 3, 0.01, -1)]
        [InlineData(-(5 / 3D) * Math.PI , - (4 / 3D) * Math.PI, 0.01, 1)]
        [InlineData(-Math.PI / 2, Math.PI/2, 0.01, 0)]
        [InlineData(-Math.PI / 2, Math.PI, 0.01, 1)]
        public void WorksWithAlternatingSinus(double a, double b, double precision, double expected)
        {
            //  arrange
            Func<double, double> func = Math.Sin;

            diCalcMethod method = diCalcMethod.cmRect;
            
            //  act            
            double actual = IntegralCalculus.Calculate(func, a, b, precision, method);

            //  assert
            Assert.InRange(actual, expected - precision, expected + precision);

            //TODO: ���������� ����� ������� �� ��� ������
            method = diCalcMethod.cmTrap;

            //  act            
            actual = IntegralCalculus.Calculate(func, a, b, precision, method);

            //  assert
            Assert.InRange(actual, expected - precision, expected + precision);
        }

        static double PolynomialFunction(double x)
        {
            return 3.45 * x * x - 2.34 * x + 12.6;
        }
    }
}
